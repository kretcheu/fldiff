//
// "$Id: FavoritesWindow.h 386 2006-03-04 14:15:27Z mike $"
//
// FavoritesWindow widget definitions.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef _FavoritesWindow_h_
#  define _FavoritesWindow_h_
#  include <FL/Fl.H>
#  include <FL/Fl_Double_Window.H>
#  include <FL/Fl_Return_Button.H>
#  include <FL/Fl_File_Browser.H>
#  include <FL/Fl_Preferences.H>


class FavoritesWindow : public Fl_Double_Window
{
  Fl_File_Browser	browser_;
  Fl_Button		up_;
  Fl_Button		delete_;
  Fl_Button		down_;
  Fl_Return_Button	ok_;
  Fl_Button		cancel_;

  static void	browser_cb(Fl_File_Browser *b, FavoritesWindow *fw);
  static void	cancel_cb(Fl_Button *b, FavoritesWindow *fw);
  static void	delete_cb(Fl_Button *b, FavoritesWindow *fw);
  static void	down_cb(Fl_Button *b, FavoritesWindow *fw);
  void		load();
  static void	ok_cb(Fl_Return_Button *b, FavoritesWindow *fw);
  void		save();
  static void	up_cb(Fl_Button *b, FavoritesWindow *fw);

  public:

		FavoritesWindow();
  void		show();
};


#endif // !_FavoritesWindow_h_

//
// End of "$Id: FavoritesWindow.h 386 2006-03-04 14:15:27Z mike $".
//
