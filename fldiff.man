.\"
.\" "$Id$"
.\"
.\"   Manual page for fldiff.
.\"
.\"   Copyright 2005 by Michael Sweet
.\"
.\"   This program is free software; you can redistribute it and/or modify
.\"   it under the terms of the GNU General Public License v2 as published
.\"   by the Free Software Foundation.
.\"
.\"   This program is distributed in the hope that it will be useful,
.\"   but WITHOUT ANY WARRANTY; without even the implied warranty of
.\"   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
.\"   GNU General Public License for more details.
.\"
.TH fldiff 1 "fldiff" "4 February 2005" "Michael Sweet"
.SH NAME
fldiff \- compare files and directories graphically
.SH SYNOPSIS
.B fldiff
[
.I first
[
.I second
] ]
.SH DESCRIPTION
\fBfldiff\fR is a graphical diff program that shows the
differences between two files/directories, or a file/directory
and a CVS or Subversion repository.
.SH COPYRIGHT
Copyright 2005 by Michael Sweet
.LP
This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License v2
as published by the Free Software Foundation.
.LP
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
.\"
.\" End of "$Id$".
.\"
