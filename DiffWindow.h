//
// "$Id: DiffWindow.h 407 2006-11-13 18:54:02Z mike $"
//
// DiffWindow widget definitions.
//
// Copyright 2005-2006 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef _DiffWindow_h_
#  define _DiffWindow_h_
#  include <FL/Fl.H>
#  include <FL/Fl_Overlay_Window.H>
#  include <FL/Fl_Menu_Bar.H>
#  include <FL/Fl_Preferences.H>
#  include "DiffView.h"


//
// DiffWindow widget to support multiple file diffs...
//

class DiffWindow : public Fl_Overlay_Window
{
  Fl_Menu_Bar	menubar_;		// Menubar
  DiffView	view_;			// Diff viewing widget
  const char	*file1_,		// First file
		*file2_;		// Second file
  DiffWindow	*next_;			// Next window
  int		search_line_;		// Current line in search
  bool		search_right_;		// Searched on the right side?
  char		search_[1024],		// Search text
		title_[1024];		// Window title
  bool		zoom_;			// Show zoom overlay?
  int		zoom_pos_;		// Zoom overlay position

  static DiffWindow	*first_;	// First window
  static const char	*help_text_;	// Help text
  static Fl_Preferences	prefs_;		// Application preferences...

  void		draw_overlay();
  void		load_prefs();
  void		open(const char *f1, const char *f2);
  void		save_prefs();

  static void	about_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	close_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	compare_selected_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	compare_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	copy_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	find_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	find_next_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static DiffWindow *find_window(const char *f1, const char *f2);
  static void	next_change_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	prefs_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	prev_change_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	quit_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	rediff_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	select_cb(DiffView *dv, DiffWindow *dw);
  static void	select_left_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	select_none_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	select_right_cb(Fl_Menu_Bar *m, DiffWindow *dw);
  static void	using_cb(Fl_Menu_Bar *m, DiffWindow *dw);

  public:

		DiffWindow(const char *file1, const char *file2);
		~DiffWindow();

  void		color(Fl_Color c) { view_.color(c); }
  Fl_Color	color() const { return (view_.color()); }
  int		handle(int event);
  bool		load(const char *file1, const char *file2);
  void		resize(int X, int Y, int W, int H);
  void		selection_color(Fl_Color c) { view_.selection_color(c); }
  Fl_Color	selection_color() const { return (view_.selection_color()); }
  void		showlinenum(bool b) { view_.showlinenum(b); }
  bool		showlinenum() const { return (view_.showlinenum()); }
  void		ignoreblanks(bool b) { view_.ignoreblanks(b); }
  bool		ignoreblanks() const { return (view_.ignoreblanks()); }
  void		tabwidth(int t) { view_.tabwidth(t); }
  int		tabwidth() const { return (view_.tabwidth()); }
  void		textcolor(Fl_Color c) { view_.textcolor(c); }
  Fl_Color	textcolor() const { return (view_.textcolor()); }
  void		textfont(uchar f) { view_.textfont(f); }
  uchar		textfont() const { return (view_.textfont()); }
  void		textsize(uchar s) { view_.textsize(s); }
  uchar		textsize() const { return (view_.textsize()); }
  void		zoom(bool b) { zoom_ = b; }
  bool		zoom() const { return (zoom_); }
};


#endif // !_DiffWindow_h_

//
// End of "$Id: DiffWindow.h 407 2006-11-13 18:54:02Z mike $".
//
