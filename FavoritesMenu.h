//
// "$Id: FavoritesMenu.h 386 2006-03-04 14:15:27Z mike $"
//
// FavoritesMenu widget header.
//
// Copyright 2005 by Michael Sweet.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License v2 as published
// by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//

#ifndef _FavoritesMenu_h_
#  define _FavoritesMenu_h_
#  include <FL/Fl.H>
#  include <FL/Fl_Menu_Button.H>
#  include <FL/Fl_Preferences.H>


class FavoritesMenu : public Fl_Menu_Button
{
  void		load_menu();

  public:

		FavoritesMenu(int X, int Y, int W, int H, const char *L = (const char *)0)
		    : Fl_Menu_Button(X, Y, W, H, L) {}

  void		add_favorite(const char *d);
  int		handle(int event);
  static void	quote(char *dst, const char *src, int dstsize);
  static void	unquote(char *dst, const char *src, int dstsize);
};


#endif // !_FavoritesMenu_h_

//
// End of "$Id: FavoritesMenu.h 386 2006-03-04 14:15:27Z mike $".
//
